<?php

namespace App\Plugins;

use App\Bean\BookBean;


interface ApiPluginInterface {


    public function findByIsbn13(string $isbn13) : ?BookBean;

    public function getName(): string;

}