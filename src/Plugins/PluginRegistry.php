<?php

namespace App\Plugins;

use Psr\Log\LoggerInterface;


class PluginRegistry {

    protected array $modules = [];
    protected LoggerInterface $logger;

    public function __construct(iterable $modules, LoggerInterface $logger)
    {
        $this->logger = $logger;

        foreach($modules as $module) {
            $this->logger->info('enregistrement du plugin ' . $module->getName());
            $this->modules[$module->getName()] = $module;
        }
    }


    /**
     * Retourne l'ensemble des modules enregistrés
     */
    public function getModules() : array {
        return $this->modules;
    }
}