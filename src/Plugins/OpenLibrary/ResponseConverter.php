<?php

namespace App\Plugins\OpenLibrary;

use DateTime;
use App\Bean\BookBean;
use App\Bean\CoverBean;
use App\Bean\AuthorBean;
use App\Bean\PublisherBean;


class ResponseConverter {

    private $publish_date_format = 'M d, Y';

    /**
     * Convert a data array to Book.
     * 
     * @see data.example.json
     * 
     */
    public function toBook(array $data) : BookBean
    {

        $book = new BookBean($data['title']);

        $book->setIsbn13($data['identifiers']['isbn_13'][0]);

        $book->setNote($data['notes']);
        $book->setNumberOfPages($data['number_of_pages']);

        foreach ($data['publishers'] as $publisher){
            $book->addPublisher(new PublisherBean($publisher['name']));
        }

        foreach ($data['authors'] as $author){
            $book->addAuthor(new AuthorBean($author['name'], $author['url']));
        }

        foreach ($data['cover'] as $key => $url){
            $book->addCover(new CoverBean($key, $url));
        }

        $book->setPublishDate(DateTime::createFromFormat($this->publish_date_format,$data['publish_date']));

        return $book;
    }

}