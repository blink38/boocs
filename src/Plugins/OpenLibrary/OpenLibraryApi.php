<?php

namespace App\Plugins\OpenLibrary;

use App\Bean\BookBean;
use App\Plugins\ApiPluginInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;


/**
 * Classe API pour l'API Open Library Books API
 * 
 * https://openlibrary.org/dev/docs/api/books
 * 
 */


class OpenLibraryApi implements ApiPluginInterface {


        private $params;
        private $client;
        private $converter;

        public function __construct (ContainerBagInterface $params, HttpClientInterface $client, ResponseConverter $converter){
            $this->client = $client;
            $this->params = $params->get('openlibrary');
            $this->converter = $converter;
        }

        /**
         * Plugin name
         */
        public function getName() : string{
            return 'openlibrary';
        }


        /**
         * Find book by isbn13
         * 
         */
        public function  findByIsbn13(String $isbn13) : ?BookBean
        {
            $url = $this->params['url'] . '/books?bibkeys=ISBN:' . $isbn13 . '&format=json&jscmd=data';
        
            $response = $this->client->request(
                'GET',
                $url
            );

            if ($response->getStatusCode() == '200' && $response->getHeaders()['content-type'][0] == 'application/json')
            {
                $content = $response->toArray();

                return $this->converter->toBook($content['ISBN:' .$isbn13]);
            }

            return null;
        }
}