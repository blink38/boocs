<?php

namespace App\Command;

use App\Plugins\PluginRegistry;
use App\Plugins\OpenLibrary\Api;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BoocsBookIsbn13Command extends Command
{
    protected static $defaultName = 'boocs:book-isbn13';

    private $registry;

    protected function configure()
    {
        $this
            ->setDescription('Recherche un livre à partir de son code ISBN13')
            ->addArgument('isbn13', InputArgument::REQUIRED, 'Code ISBN13')
            ->addOption('import', 'i', InputOption::VALUE_NONE, 'Importe le résultat en base de données')
        ;
    }

    public function __construct(PluginRegistry $registry){

        $this->registry = $registry;        
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $isbn13 = $input->getArgument('isbn13');


        $content = null;
        
        foreach ($this->registry->getModules() as $module){

            $content = $module->findByIsbn13($isbn13);
            if (!empty($content)){

                $io->info($module->getName() . ' found : ' . $content->getTitle());
                break;
            }
        }

        if ($content != null && $input->getOption('import')){

            // importation du résultat en base de données
        }

        return Command::SUCCESS;
    }
}
