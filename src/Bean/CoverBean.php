<?php

namespace App\Bean;

class CoverBean {


    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $uri;


    public function __construct(string $type, string $uri){
        $this->type = $type;
        $this->uri = $uri;
    }
    /**
     * Get the value of type
     *
     * @return  string
     */ 
    public function getType() : ?string
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @param  string  $type
     *
     * @return  self
     */ 
    public function setType(string $type) :self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of uri
     *
     * @return  string
     */ 
    public function getUri() : ?string
    {
        return $this->uri;
    }

    /**
     * Set the value of uri
     *
     * @param  string  $uri
     *
     * @return  self
     */ 
    public function setUri(string $uri) :self
    {
        $this->uri = $uri;

        return $this;
    }
}