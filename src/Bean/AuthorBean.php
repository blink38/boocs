<?php

namespace App\Bean;


class AuthorBean {

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;
    

    public function __construct(string $name, string $url){
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * Get the value of name
     *
     * @return  string
     */ 
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param  string  $name
     *
     * @return  self
     */ 
    public function setName(string $name) :self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of url
     *
     * @return  string
     */ 
    public function getUrl() :?string
    {
        return $this->url;
    }

    /**
     * Set the value of url
     *
     * @param  string  $url
     *
     * @return  self
     */ 
    public function setUrl(string $url) :self
    {
        $this->url = $url;

        return $this;
    }
}