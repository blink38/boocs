<?php

namespace App\Bean;

use DateTime;


class BookBean {


    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $isbn13;
    /**
     * @var string
     */
    private $isbn10;

    /**
     * @var string
     */
    private $note;
    /**
     * @var int
     */
    private $numberOfPages;

    /**
     * @var DateTime
     */
    private $publishDate;
    
    /**
     * @var AuthorBean[]
     */
    private $authors = [];

    /**
     * @var PublisherBean[]
     */
    private $publishers = [];

    /**
     * @var Category[]
     */
    private $categories = [];

    /**
     * @var Cover[]
     */
    private $covers = [];


    public function __construct(string $title){
        $this->title = $title;
    }


    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get the value of url
     *
     * @return  string
     */ 
    public function getUrl() : ?string
    {
        return $this->url;
    }

    /**
     * Set the value of url
     *
     * @param  string  $url
     *
     * @return  self
     */ 
    public function setUrl(string $url) :self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get the value of numberOfPages
     *
     * @return  int
     */ 
    public function getNumberOfPages() :?int
    {
        return $this->numberOfPages;
    }

    /**
     * Set the value of numberOfPages
     *
     * @param  int  $numberOfPages
     *
     * @return  self
     */ 
    public function setNumberOfPages(int $numberOfPages) :self
    {
        $this->numberOfPages = $numberOfPages;

        return $this;
    }

    /**
     * Get the value of isbn13
     *
     * @return  string
     */ 
    public function getIsbn13() :?string
    {
        return $this->isbn13;
    }

    /**
     * Set the value of isbn13
     *
     * @param  string  $isbn13
     *
     * @return  self
     */ 
    public function setIsbn13(string $isbn13) :self
    {
        $this->isbn13 = $isbn13;

        return $this;
    }

    /**
     * Get the value of isbn10
     *
     * @return  string
     */ 
    public function getIsbn10() :?string
    {
        return $this->isbn10;
    }

    /**
     * Set the value of isbn10
     *
     * @param  string  $isbn10
     *
     * @return  self
     */ 
    public function setIsbn10(string $isbn10) :self
    {
        $this->isbn10 = $isbn10;

        return $this;
    }

    /**
     * Get the value of note
     *
     * @return  string
     */ 
    public function getNote() :?string
    {
        return $this->note;
    }

    /**
     * Set the value of note
     *
     * @param  string  $note
     *
     * @return  self
     */ 
    public function setNote(string $note) :self
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get the value of authors
     *
     * @return  AuthorBean[]
     */ 
    public function getAuthors() : ?Array
    {
        return $this->authors;
    }

    /**
     * Set the value of authors
     *
     * @param  AuthorBean[]  $authors
     *
     * @return  self
     */ 
    public function setAuthors(Array $authors) :self
    {
        $this->authors = $authors;

        return $this;
    }

    /** 
     * Add an author to the list
     */
    public function addAuthor(AuthorBean $author) :self
    {
        $this->authors[] = $author;
        return $this;
    }

    /**
     * Get the value of publishers
     *
     * @return  PublisherBean[]
     */ 
    public function getPublishers() :?Array
    {
        return $this->publishers;
    }

    /**
     * Set the value of publishers
     *
     * @param  PublisherBean[]  $publishers
     *
     * @return  self
     */ 
    public function setPublishers(Array $publishers) :self
    {
        $this->publishers = $publishers;

        return $this;
    }

     /**
     * Add a publisher to the list
     */
    public function addPublisher(PublisherBean $publisher) :self
    {
        $this->publishers[] = $publisher;
        return $this;
    }

    /**
     * Get the value of categories
     *
     * @return  Category[]
     */ 
    public function getCategories() :?Array
    {
        return $this->categories;
    }

    /**
     * Set the value of categories
     *
     * @param  Category[]  $categories
     *
     * @return  self
     */ 
    public function setCategories(Array $categories) :self
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Add a category to the list
     */
    public function addCategory(CategoryBean $category) :self
    {
        $this->categories[] = $category;
        return $this;
    }

    /**
     * Get the value of covers
     *
     * @return  Cover[]
     */ 
    public function getCovers() :?Array
    {
        return $this->covers;
    }

    /**
     * Set the value of covers
     *
     * @param  Cover[]  $covers
     *
     * @return  self
     */ 
    public function setCovers(Array $covers) :self
    {
        $this->covers = $covers;

        return $this;
    }


    /**
     * Add a cover in list
     */
    public function addCover(CoverBean $cover) :self
    {
        $this->covers[] = $cover;
        return $this;
    }


    /**
     * Get the value of publishDate
     *
     * @return  DateTime
     */ 
    public function getPublishDate() : ?DateTime
    {
        return $this->publishDate;
    }

    /**
     * Set the value of publishDate
     *
     * @param  DateTime  $publishDate
     *
     * @return  self
     */ 
    public function setPublishDate(?DateTime $publishDate) :self
    {
        if ($publishDate !== false){
            $this->publishDate = $publishDate;
        }

        return $this;
    }
}